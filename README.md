# Perturbative Expansion in Loop Quantum Gravity coupled to massless scalar field

## Description
In a model of Loop Quantum Gravity coupled to massless scalar field proposed in https://arxiv.org/pdf/1009.2445.pdf a technical problem is to find an exponential of imaginary unit times a square root of an operator (gravitational part of the scalar constraint) times a number (scalar field value). We will call it an evolution matrix. This software provides tools to calculate a perturbative expansion of the evolution matrix (similar in spirit to the one considered in https://arxiv.org/pdf/1001.5147.pdf). We consider a splitting T=D+K of the operator T defining the evolution into a part D which is easy to diagonalize and the rest called K. The input to the algorithm are:
1. The vector of the eigenvalues of D.
2. The matrix of K in the eigenbasis of D.
3. Time parameter (Dphi).
4. Order N of the expansion.
The output is the perturbative evolution matrix calculated up to order N. The program uses arbitrary precision arithmetics to calculate the expansion.

## Installation
The software is available under Linux only. It uses the library CLN for arbitrary precision arithmetics https://www.ginac.de/CLN/ . It needs to be installed.
Under Linux system type:
make 
This will create a file generate_perturbative_matrix in the /bin folder.

## Usage
Run the binary file generate_perturbative_matrix with the following parameters:
R: the rank of the matrix T (non-negative integer).
O: the order of the expansion (non-negative integer).
d: time parameter dphi (real number).
P: precision used in the calculations (how many significant digits should be used).
F: the name of a file containing a vector of real numbers: the eigenvalues of D.
I: the name of a file containing the matrix of K in the D eigenbasis.
f: the output file name.
Example usage:
./bin/generate_perturbative_matrix -R 100 -O 2 -P 20 -d 0.001 -F ./data/free_part.dat -I ./data/interacting_part.dat -f ./data/perturbative_evolution_matrix_up_to_order_2.dat
Examples of files are available under ./data directory. The interacting part and free part are generated for and Loop Quantum Cosmology operator Theta (the operator is described in https://arxiv.org/pdf/1112.0360.pdf). The cosmological constant value is chosen to be 0.01 (in Planck units) in this example. The matrix is calculated for volume v in the interval [4.0,400.0] (see https://arxiv.org/pdf/1001.5147.pdf for the relevant formulas in this case). The implementation here is general and is not optimized for Loop Quantum Cosmology (in LQC the K matrix is sparse which can be used to limit possible histories in the expansion) and notice that running the program for order at least 3 is already challenging (it took around 3h on my laptop). 

The intended usage is not LQC but LQG where the operator T is split into D+K, where the matrix K is dense in the D eigenbasis. We expect that such splitting takes place if one considers a splitting of the gravitational part of the scalar constraint into homogeneous-isotropic part proposed in https://arxiv.org/pdf/2211.04440.pdf and the rest. The usage scenrio is to calculate the evolution of cosmological perturbations.

## Support
For support please contact me on Marcin.Kisielowski@gmail.com.

## Authors and acknowledgment
The author of this project is Marcin Kisielowski.

## License
The software is available under GPLv3 licence.
