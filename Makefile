CC=g++
# CFLAGS= -Wall -O3 -ffast-math -frepo -I../Library
SRC_PATH=./src
INC_PATH=./include
OBJ_PATH=./obj
BIN_PATH=./bin

CFLAGS= -O3 -Wall -I$(INC_PATH) -I$(CLN_HOME)/include
LDLIBS= -L$(CLN_HOME)/lib -lm -lcln
COBJS= $(OBJ_PATH)/evolution-matrix.o $(OBJ_PATH)/histories.o $(OBJ_PATH)/jet.o $(OBJ_PATH)/partitions.o
all: generate_perturbative_matrix

generate_perturbative_matrix: $(OBJ_PATH)/generate_perturbative_matrix.o $(COBJS)
	mkdir -p $(BIN_PATH)
	$(CC) $(CFLAGS) -o $(BIN_PATH)/$@ $^ $(LDLIBS)

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.cpp
	mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(BIN_PATH)/* $(OBJ_PATH)/*

install:
	rm -f *.rpo $(COBJS)
