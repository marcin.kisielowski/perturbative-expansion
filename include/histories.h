#ifndef __histories_h__
#define __histories_h__
#include<vector>
#include<algorithm>


class Histories{
    public:
    Histories(std::vector<int> & step, int number_of_steps ); // 
    std::vector<int> Step;
    int NumberOfSteps;
    class iterator: public std::iterator<std::input_iterator_tag, std::vector<int>, std::vector<int>, std::vector<int>, std::vector<int> >{
        public:
        explicit iterator();
        iterator(std::vector<int> & step, int length);//Creates the beginning history
        iterator(std::vector<int> & step, const std::vector<int > & history);
        std::vector<int> Step;
        int Length; //Length of the history
        int Size; //Size of the step vector
        std::vector< int > HistoryVector;
        iterator & operator++();
        iterator operator++(int);
        bool operator==(iterator & other) const;
        bool operator!=(iterator & other) const;
        std::vector<int> operator*() const;
    };
    iterator Begin;
    iterator End;
    iterator & begin();
    iterator & end();
};




#endif