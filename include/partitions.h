#ifndef __partitions_h__
#define __partitions_h__
#include<vector>
#include<map>

namespace partitions{

class PartitionsOfNumber{
  public:
  PartitionsOfNumber(int number=0);
  void Generate_Partitions(int number);
  int n;
  std::vector<std::vector<int> > Elements;    
};

class ConfigurationsForNumber{
    public:
    ConfigurationsForNumber(const PartitionsOfNumber partitions);
    ConfigurationsForNumber(int number);
    int n;
    std::vector<std::map<int,int> > Elements;
};

//std::vector<std::vector<int> > Generate_Partitions(int number);
std::map<int,int> partition_to_configuration(std::vector<int> partition);

class Partitions{
public:
Partitions(int number=0); //The constructor generates all partitions up to number
std::vector<PartitionsOfNumber> Elements;
};

class Configurations{
    public:
Configurations(const Partitions partitions);
Configurations(int number=0);
std::vector<ConfigurationsForNumber> Elements;
};

/*
class WorkSpace{
public:
    WorkSpace(int number);
    Partitions all_partitions;
    Configurations all_configurations;
}; */

}
#endif