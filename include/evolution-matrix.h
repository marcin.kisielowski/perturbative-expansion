#ifndef __evolution_matrix_h__
#define __evolution_matrix_h__

#include <cln/cln.h>
#include <algorithm>
#include "histories.h"
#include "jet.h"
#include<complex>

typedef long double ld;
typedef std::complex<long double> ldc;

cln::cl_RA ld2clRA(ld value);
ldc clN2ldc(cln::cl_N value);
std::vector<cln::cl_RA> vld2vclRA(std::vector<ld> value);
std::vector<ldc> vclN2vldc(std::vector<cln::cl_N> value);

class PerturbativeEvolutionMatrix{
	public:
	PerturbativeEvolutionMatrix(ld dphi, int order, int rank);
	void Generate(int m, int n);
	void Store(std::string file_name);
	void StoreAbs(std::string file_name);
	cln::cl_N GetMatrixElement(int i, int j);
	protected:
	cln::cl_N GetEdgesAmplitude(const std::map<cln::cl_RA, int> & poles );
	cln::cl_RA GetVerticesAmplitude(std::vector<int> path);
	virtual cln::cl_RA GetFreePart(int i)=0;
	virtual cln::cl_RA GetInteractingPart(int i, int j)=0;
	cln::cl_RA Dphi;
	int Order;	
	int Rank; // Rank of the 
	std::vector<int> Step;
	std::vector<cln::cl_N > Matrix;
	int M,N;
};

class TrivialPerturbativeEvolutionMatrix:public PerturbativeEvolutionMatrix{
	public:
	TrivialPerturbativeEvolutionMatrix(ld dphi, int order, int rank);
	protected:
	cln::cl_RA GetFreePart(int i) final;
	cln::cl_RA GetInteractingPart(int i, int j) final;
};

class LQCPerturbativeEvolutionMatrix:public PerturbativeEvolutionMatrix{
	public:
	LQCPerturbativeEvolutionMatrix(cln::cl_RA dphi, cln::cl_RA lambda, int order, int rank);
	protected:
	cln::cl_RA GetFreePart(int i) final;
	cln::cl_RA GetInteractingPart(int i, int j) final;
};

class GeneralPerturbativeEvolutionMatrix:public PerturbativeEvolutionMatrix{
	public:
	GeneralPerturbativeEvolutionMatrix(ld dphi, int order,int rank);
	void LoadFreePart(std::string file_name);
	void LoadFreePart(const std::vector<ld> & free_part);
	void LoadInteractingPart(std::string file_name);
	void LoadInteractingPart(const std::vector<ld> & interacting_part);
	protected:
	cln::cl_RA GetFreePart(int i) final;
	cln::cl_RA GetInteractingPart(int i, int j) final;
	std::vector<cln::cl_RA> FreePart;
	std::vector<cln::cl_RA> InteractingPart;
};

#endif