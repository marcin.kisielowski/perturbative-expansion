#ifndef __jet_h__
#define __jet_h__

#include<iostream>
#include<vector>
#include<map>
#include<cln/io.h>
#include<cln/output.h>
#include<cln/ring.h>
#include<cln/univpoly.h>
#include<cln/univpoly_rational.h>
#include<cln/number.h>
#include<cln/rational.h>
#include <cln/integer.h>
#include <cln/rational_io.h>
#include<cln/complex.h>
#include<cln/complex_io.h>
#include<cln/float.h>
#include<cln/float_io.h>
#include <cln/real.h>
#include <cln/complex.h>
#include"partitions.h"

namespace jet{

//extern int Precision=10; //Precision for numerical calculation
//extern partitions::Configurations work_space; //This will have to be intitialized before calculations

class parameters
{
    public:
        static int precision;
        static partitions::Configurations configurations;
       static parameters& getInstance()
        {
            static parameters    instance; // Guaranteed to be destroyed.
                                  // Instantiated on first use.
            return instance;
        }
    private:
        parameters() {}                    // Constructor? (the {} brackets) are needed here.

        // C++ 03
        // ========
        // Don't forget to declare these two. You want to make sure they
        // are unacceptable otherwise you may accidentally get copies of
        // your singleton appearing.
        parameters(parameters const&);              // Don't Implement
        void operator=(parameters const&); // Don't implement

        // C++ 11
        // =======
        // We can use the better technique of deleting the methods
        // we don't want.
    public:
       // parameters(parameters const&)               = delete;
       // void operator=(parameters const&)  = delete;

        // Note: Scott Meyers mentions in his Effective Modern
        //       C++ book, that deleted functions should generally
        //       be public as it results in better error messages
        //       due to the compilers behavior to check accessibility
        //       before deleted status
};

//extern parameters work_space;

extern parameters & work_space;

template <class Ring>
class FunctionJet{
	public:
    FunctionJet();
	FunctionJet(int order, Ring point);
	int Order;
	Ring Point;
	std::vector<Ring> Derivative;
	FunctionJet<Ring> operator[](const FunctionJet<Ring> & g);
};

class ReciprocalFunctionJet: public FunctionJet<cln::cl_RA>{
	public:
	ReciprocalFunctionJet(int order, cln::cl_RA point);
};

class PolynomialJet:public FunctionJet<cln::cl_RA>{
	public:
	PolynomialJet(int order, cln::cl_RA point, const cln::cl_UP_RA & polynomial );
	PolynomialJet(int order, cln::cl_RA point, const std::map<cln::cl_RA, int> & poles );
	cln::cl_univpoly_rational_ring PolynomialRing;
	cln::cl_UP_RA Polynomial;
};

class ExponentialFunctionJet: public FunctionJet<cln::cl_N>{
	public:
	ExponentialFunctionJet(int order, cln::cl_N point, cln::cl_RA dphi);
	cln::cl_RA Dphi;
};

class SquareRootJet: public FunctionJet<cln::cl_N>{
	public:
	SquareRootJet(int order, cln::cl_RA point);
};

template<class Ring>
Ring FaaDiBrunoFormula(const FunctionJet<Ring> & f, const FunctionJet<Ring> & g, int DerivativeOrder);

template<class Ring1, class Ring2> 
Ring2 LeibnitzRule(const FunctionJet<Ring1> & f, const FunctionJet<Ring2> & g, int DerivativeOrder);

}

#endif