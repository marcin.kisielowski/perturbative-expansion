#include"evolution-matrix.h"
#include <unistd.h> //getopt

int main(int argc, char *argv[]){
   int opt;
   ld Dphi;
   int Order,Rank,Precision;
   std::string OutFileName,FreePartFileName,InteractingPartFileName;
    
   while ((opt = getopt (argc, argv, "R:O:P:d:f:F:I:")) != -1){
    switch (opt)
      {
      case 'R':
        Rank=atoi(argv[optind-1]);
        break;
      case 'O':
        Order=atoi(argv[optind-1]);
        break;
      case 'P':
        Precision=atoi(argv[optind-1]);
        break;
      case 'd':
        Dphi=atof(argv[optind-1]);
        break;
      case 'f':
        OutFileName=argv[optind-1];
        break;
      case 'F':
        FreePartFileName=argv[optind-1];
        break;
      case 'I':
        InteractingPartFileName=argv[optind-1];
        break;
      case '?':
       if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort ();
      }}
    
    jet::work_space.precision=Precision;
    jet::work_space.configurations=Order;
    GeneralPerturbativeEvolutionMatrix U(Dphi, Order,Rank);
    U.LoadFreePart(FreePartFileName);
    U.LoadInteractingPart(InteractingPartFileName);
    U.Generate(Rank,Rank);
    U.Store(OutFileName);
}