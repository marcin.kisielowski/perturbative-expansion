#include "evolution-matrix.h"
#include <iostream>
#include <fstream>
#include <cln/complex.h>
#include <algorithm>
#include <math.h>
#include <numeric>
#include <iomanip>
#include <ctype.h>
#define debug false


cln::cl_RA ld2clRA(ld value){
	std::stringstream stream;
    stream << std::fixed << std::setprecision(std::numeric_limits<long double>::digits10 + 1) << value<< '_'<<std::numeric_limits<long double>::digits10 + 1 ;
    return cln::rational(cln::cl_F(stream.str().c_str()));
}

ld clR2ld(cln::cl_R value){
	std::stringstream stream;
	stream << cln::cl_float(value, cln::float_format(jet::parameters::precision));
    std::string string_number = stream.str();
	auto position = std::find_if(string_number.begin(),string_number.end(),isalpha);
	if(position == string_number.end()) {std::cout << "WARNING: in clR2ld. Letter not found." << std::endl; 
	std::cout <<"value=" << value << std::endl;
	std::cout <<"string_number=" << string_number <<std::endl;
	}
	else *position='E';
    return std::stold(string_number,nullptr);
}

ldc clN2ldc(cln::cl_N value){
	ld realpart;
	ld imaginarypart;
	realpart= clR2ld(cln::realpart(value));
	imaginarypart= clR2ld(cln::imagpart(value));
	return ldc(realpart,imaginarypart);
}

std::vector<cln::cl_RA> vld2vclRA(std::vector<ld> ld_vector){
	std::vector<cln::cl_RA> clRA_vector(ld_vector.size());
	for(size_t i=0; i< ld_vector.size();++i){
		clRA_vector[i]=ld2clRA(ld_vector[i]);
	}
	return clRA_vector;
}

std::vector<ldc> vclN2vldc(std::vector<cln::cl_N> clN_vector){
	std::vector<ldc> ldc_vector(clN_vector.size());
	for(size_t i=0; i< clN_vector.size();++i){
		ldc_vector[i]=clN2ldc(clN_vector[i]);
	}
	return ldc_vector;
}


PerturbativeEvolutionMatrix::PerturbativeEvolutionMatrix(ld dphi, int order, int rank){
	Dphi=ld2clRA(dphi);
	Rank=rank;
	Order=order;
	Step.resize(rank);
	for(int i=0;i<rank;++i) Step[i]=i;
	M=0;
	N=0;
}

void PerturbativeEvolutionMatrix::Generate(int m, int n){
	M=m;
	N=n;
	Matrix.resize(M*N);
	for(int i=0;i<M;++i){
		for(int j=0;j<N;++j){
			Matrix[i*N+j]=GetMatrixElement(i,j);
		}
	}
}

void PerturbativeEvolutionMatrix::Store(std::string file_name){
	std::ofstream file_stream(file_name);
	for(int i=0;i<M;++i){
		for(int j=0;j<N;++j){
			file_stream << clN2ldc(Matrix[i*N+j]) << '\t';
		}
		file_stream << '\n';
	}
}

void PerturbativeEvolutionMatrix::StoreAbs(std::string file_name){
	std::ofstream file_stream(file_name);
	for(int i=0;i<M;++i){
		for(int j=0;j<N;++j){
			file_stream << std::abs(clN2ldc(Matrix[i*N+j])) << '\t';
		}
		file_stream << '\n';
	}
}

cln::cl_N PerturbativeEvolutionMatrix::GetMatrixElement(int i, int j){
	cln::cl_N MatrixElement=0;
	if(i==j) {
		std::map<cln::cl_RA, int> poles;
		poles[GetFreePart(i)]=1;
		MatrixElement=GetEdgesAmplitude(poles);
	}
	for(int number_of_steps=0;number_of_steps<Order;++number_of_steps){
		Histories Paths(Step, number_of_steps);
		for(auto path:Paths){
			path.insert(path.begin(),i);
			path.push_back(j);
			std::map<int,int> Degeneracy;
			for(auto state:path){
				Degeneracy[state]++;
			}
			std::map<cln::cl_RA, int> poles;
			for(auto deg: Degeneracy){
				poles[GetFreePart(deg.first)]=deg.second;
			} 
			MatrixElement+=GetEdgesAmplitude(poles)*GetVerticesAmplitude(path);
		}
	}
	return MatrixElement;
}

cln::cl_N PerturbativeEvolutionMatrix::GetEdgesAmplitude(const std::map<cln::cl_RA, int> & poles ){
	cln::cl_N Result =0;
	std::map<cln::cl_RA, int> Poles=poles;
	for(auto poleit=Poles.begin(); poleit != Poles.end(); ++poleit  ){
		auto pole = *poleit;
		poleit=Poles.erase(poleit);
		int DerivativeOrder=pole.second-1;
		cln::cl_RA Point = pole.first;
		jet::PolynomialJet polynomial_function(DerivativeOrder, Point, Poles );
		jet::ReciprocalFunctionJet reciprocal_function(DerivativeOrder, polynomial_function.Derivative[0]);
		jet::SquareRootJet sqrt_function(DerivativeOrder, Point);
		jet::ExponentialFunctionJet exp_function(DerivativeOrder, sqrt_function.Derivative[0], Dphi);
		Result+= 1/cln::factorial(DerivativeOrder) * jet::LeibnitzRule<cln::cl_RA,cln::cl_N>(reciprocal_function[polynomial_function],exp_function[sqrt_function],DerivativeOrder);
		poleit=Poles.insert(poleit,pole);
	}
	return Result;
}

cln::cl_RA PerturbativeEvolutionMatrix::GetVerticesAmplitude(std::vector<int> path){
	cln::cl_RA ProductOfVertexAmplitudes=1;
	for(int i=0;i<path.size()-1;++i){
		ProductOfVertexAmplitudes*=GetInteractingPart(path[i+1],path[i]);
	}
	return ProductOfVertexAmplitudes;
}

TrivialPerturbativeEvolutionMatrix::TrivialPerturbativeEvolutionMatrix(ld dphi, int order, int rank):PerturbativeEvolutionMatrix(dphi,order,rank){
;
}

cln::cl_RA TrivialPerturbativeEvolutionMatrix::GetFreePart(int i){
	cln::cl_RA numerator=2*(i+1);
	cln::cl_RA denominator=Rank;
	cln::cl_RA return_value=ld2clRA(M_PI)*numerator/denominator;
	return return_value*return_value;
}

cln::cl_RA TrivialPerturbativeEvolutionMatrix::GetInteractingPart(int i, int j){
	return 0;
}

GeneralPerturbativeEvolutionMatrix::GeneralPerturbativeEvolutionMatrix(ld dphi, int order, int rank):PerturbativeEvolutionMatrix(dphi,order,rank){;}

void GeneralPerturbativeEvolutionMatrix::LoadFreePart(std::string file_name){
	std::ifstream file_stream(file_name);
	std::vector<ld> FreePartLd(Rank);
	for(int i=0; i<Rank; ++i){
		file_stream >> FreePartLd[i];
	}
	LoadFreePart(FreePartLd);
}

void GeneralPerturbativeEvolutionMatrix::LoadFreePart(const std::vector<ld> & freepart){
	FreePart = vld2vclRA(freepart);
}

void GeneralPerturbativeEvolutionMatrix::LoadInteractingPart(std::string file_name){
	std::ifstream file_stream(file_name);
	std::vector<ld> InteractingPartLd(Rank*Rank);
	for(int i=0; i<Rank*Rank; ++i){
		file_stream >> InteractingPartLd[i];
	}
	LoadInteractingPart(InteractingPartLd);
}

void GeneralPerturbativeEvolutionMatrix::LoadInteractingPart(const std::vector<ld> & interactingpart){
	InteractingPart = vld2vclRA(interactingpart);
}

cln::cl_RA GeneralPerturbativeEvolutionMatrix::GetFreePart(int i){
	return FreePart[i];
}

cln::cl_RA GeneralPerturbativeEvolutionMatrix::GetInteractingPart(int i,int j){
	return InteractingPart[i*Rank+j];
}
