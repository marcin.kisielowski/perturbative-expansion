#include"partitions.h"
#include<iostream>

void partitions::PartitionsOfNumber::Generate_Partitions(int number)
{
	int q=0;
	std::vector<int> partition (1,number);
	Elements.push_back(partition);
	if(number==1) q--;
	while(q>=0){
	if(partition[q]==2){
			partition[q]=1; 
			q--;
			partition.push_back(1);
			Elements.push_back(partition);
	}
	else{
		int remainder= partition.size()-q;
		int x=partition[q]-1;
		partition[q]=x;
		partition.resize(q+1);
		while(remainder > x) {partition.push_back(x); remainder-=x; q++;}
		if(remainder) partition.push_back(remainder);
		if(remainder>1) q++;
		Elements.push_back(partition);
	}
	}
	return ;
}

 std::map<int,int> partitions::partition_to_configuration(std::vector<int> partition){
	 std::map<int,int> result;
	 for(auto p:partition){
		 result[p]++;
	 }
	 return result;
 }

partitions::PartitionsOfNumber::PartitionsOfNumber(int number){
    n=number;
    if(n>0) Generate_Partitions(n);
}

partitions::ConfigurationsForNumber::ConfigurationsForNumber(const PartitionsOfNumber partitions_of_number){
    for(auto partition: partitions_of_number.Elements)
    Elements.push_back(partition_to_configuration(partition));
}

partitions::ConfigurationsForNumber::ConfigurationsForNumber(int number):ConfigurationsForNumber(PartitionsOfNumber(number)){
;
}

partitions::Partitions::Partitions(int number){
    for(int i=0; i<=number; ++i){
        Elements.push_back(PartitionsOfNumber(i));
    }
}

partitions::Configurations::Configurations(const partitions::Partitions allpartitions){
    for(auto partition_of_number: allpartitions.Elements){
        Elements.push_back(ConfigurationsForNumber(partition_of_number));
    }
}

partitions::Configurations::Configurations(int number): partitions::Configurations::Configurations(partitions::Partitions(number)){
    ;
}