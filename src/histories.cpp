#include"histories.h"
#include<iostream>

Histories::iterator::iterator(){
    Length=0;
    Size=0;
}

Histories::iterator::iterator(std::vector<int> & step, int length){
    Step=step;
    Size=Step.size();
    Length=length;
    HistoryVector=std::vector<int>(Length, 0);
}

Histories::iterator::iterator(std::vector<int> & step, const std::vector< int > & history){
    Step=step;
    Size=Step.size();
    HistoryVector=history;
    Length=HistoryVector.size();
}

Histories::iterator & Histories::iterator::operator++(){
    for(auto it=HistoryVector.rbegin(); it!=HistoryVector.rend(); ++it){
        if(*it < Size-1) {
            (*it)++;
        for(auto zeroit=HistoryVector.rbegin(); zeroit != it; ++zeroit) *zeroit=0;
        return *this;}
    }
    HistoryVector = std::vector<int>(Length+1,0);
    return *this;
}

Histories::iterator Histories::iterator::operator++(int ){
    Histories::iterator Result=*this;
    ++Result;
    return Result;
}

bool Histories::iterator::operator==(Histories::iterator & other) const{
    return HistoryVector==other.HistoryVector;
}

bool Histories::iterator::operator!=(Histories::iterator & other) const{
    return HistoryVector!=other.HistoryVector;
}

std::vector<int> Histories::iterator::operator*() const{
    std::vector<int> Result;
    for(const auto & v: HistoryVector){
        Result.push_back(Step[v]);
    }
    return Result;
}

Histories::Histories(std::vector<int> & step, int number_of_steps ){
    Step=step;
    NumberOfSteps=number_of_steps;
    Begin=iterator(Step,number_of_steps);
    End=iterator(Step,number_of_steps+1);
}

Histories::iterator & Histories::begin(){
    return Begin;
}

Histories::iterator & Histories::end(){
    return End;
}