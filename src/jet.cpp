#include"jet.h"

using namespace jet;

//parameters work_space;
int parameters::precision=10;
partitions::Configurations parameters::configurations=0;
parameters & work_space = parameters::getInstance();

template <class Ring>
FunctionJet<Ring> FunctionJet<Ring>::operator[](const FunctionJet<Ring> & g){
	FunctionJet<Ring> Result(Order,g.Point);
	for(int i=0;i<=Order; ++i) Result.Derivative.push_back(FaaDiBrunoFormula(*this,g,i));
	return Result;
}

template<class Ring>
FunctionJet<Ring>::FunctionJet(){
;
}

template<class Ring>
FunctionJet<Ring>::FunctionJet(int order, Ring point){
	Order=order;
	Point=point;
}

ReciprocalFunctionJet::ReciprocalFunctionJet(int order, cln::cl_RA point):FunctionJet<cln::cl_RA>(order,point){
	for(int m=0; m<=Order;++m){
		Derivative.push_back( (-1)*cln::factorial(m)*expt(-1/Point, m+1)  );
	}
}

PolynomialJet::PolynomialJet(int order, cln::cl_RA point, const cln::cl_UP_RA & polynomial ):FunctionJet<cln::cl_RA>(order,point),Polynomial(polynomial){
	PolynomialRing = Polynomial.ring();
	Polynomial=polynomial;
	cln::cl_UP_RA DPolynomial= Polynomial;
	for(int m=0;m<=Order; ++m){
		Derivative.push_back(DPolynomial(Point));
		DPolynomial=deriv(DPolynomial);
	}
}

PolynomialJet::PolynomialJet(int order, cln::cl_RA point, const std::map<cln::cl_RA, int> & poles  ):FunctionJet<cln::cl_RA>(order,point),PolynomialRing(find_univpoly_ring (cln::cl_RA_ring)),Polynomial(PolynomialRing->one()){
	Polynomial=PolynomialRing->one();
	for(auto pole:poles) {
		cln::cl_UP_RA Factor(PolynomialRing->create(1));
		cln::cl_RA pole_value=-pole.first;
		set_coeff(Factor,0,pole_value);
		set_coeff(Factor,1,1);
		finalize(Factor);
		Polynomial = Polynomial * expt_pos(Factor,pole.second);
		}
		cln::cl_UP_RA DPolynomial= Polynomial;
	for(int m=0;m<=Order; ++m){
		Derivative.push_back(DPolynomial(Point));
		DPolynomial=deriv(DPolynomial);
	}
}

ExponentialFunctionJet::ExponentialFunctionJet(int order, cln::cl_N point, cln::cl_RA dphi):FunctionJet<cln::cl_N>(order,point){
	Dphi=dphi;
	cln::cl_N iu= cln::complex(0, 1);
	for(int m=0;m<=Order; ++m){
		Derivative.push_back( expt(iu * Dphi, m)* exp(iu*Point*Dphi) );
	}
}

SquareRootJet::SquareRootJet(int order, cln::cl_RA point):FunctionJet<cln::cl_N>(order,cln::cl_float(point,cln::float_format(parameters::precision))){
	Derivative.push_back(sqrt(Point));
	if(Order >0) Derivative.push_back(1/(2*sqrt( Point)));
	for(int m=2; m<=Order;++m){
		Derivative.push_back( (-1)*expt(-1/(2*Point), m)*cln::doublefactorial(2*m-3)*sqrt(Point) );
	}
}

template<class Ring>
Ring jet::FaaDiBrunoFormula(const FunctionJet<Ring> & f, const FunctionJet<Ring> & g, int DerivativeOrder){
	if(f.Point!=g.Derivative[0]) {std::cerr << "ERROR: In Faa Di Bruno formula. In the composite function f(y), y!=g(x). Returning 0." <<std::endl;
	std::cout << "y=" << f.Point << " g(x)=" << g.Derivative[0] << std::endl;
	return 0;}
	if(f.Order<DerivativeOrder && f.Order<DerivativeOrder)  {std::cerr << "ERROR: In Faa Di Bruno formula. The order of the jets needs to be at least equal to DerivativeOrder. Returning 0." <<std::endl;
	return 0;}
	Ring Result=0;
	if(DerivativeOrder==0){Result=f.Derivative[0]; return Result;}
    if(parameters::configurations.Elements.size() <= DerivativeOrder) {std::cout << "ERROR: In Faa Di Bruno formula. The workspace not initialized. Returning 0." << std::endl;
    std::cout << "works_space size = " << parameters::configurations.Elements.size() << " DerivativeOrder = " << DerivativeOrder << std::endl; return Result;
    }
	for(auto configuration: parameters::configurations.Elements[DerivativeOrder].Elements){
		int sum_mj=0;
		Ring Term=cln::factorial(DerivativeOrder);
		for(auto mj:configuration){
			Term*=1/cln::factorial(mj.second);
			Term*=expt(g.Derivative[mj.first]/cln::factorial(mj.first),mj.second);
			sum_mj+=mj.second;
		}
		Term*=f.Derivative[sum_mj];
		Result+=Term;
	}
	return Result;
}

template<class Ring1, class Ring2> 
Ring2 jet::LeibnitzRule(const FunctionJet<Ring1> & f, const FunctionJet<Ring2> & g, int DerivativeOrder){ //Return type always Ring2
	if(cln::double_approx(abs(f.Point-g.Point))>1e-6* cln::double_approx(abs(g.Point))) {std::cerr << "ERROR: In Leibnitz rule. Derivatives calculated at different points. Returning 0." <<std::endl;
	std::cout << "x1=" << f.Point << " x2=" << g.Point << std::endl;
	return 0;}
	if(f.Order<DerivativeOrder && f.Order<DerivativeOrder)  {std::cerr << "ERROR: In Leibnitz rule. The order of the jets needs to be at least equal to DerivativeOrder. Returning 0." <<std::endl;
	return 0;}
	Ring2 Result=0;
	for(int k=0; k<=DerivativeOrder; ++k){
		Result+=cln::binomial(DerivativeOrder, k)*f.Derivative[DerivativeOrder-k]*g.Derivative[k];
	}
	return Result;
}

template class jet::FunctionJet<cln::cl_N>;
template class jet::FunctionJet<cln::cl_RA>;
template cln::cl_RA FaaDiBrunoFormula<cln::cl_RA>(const FunctionJet<cln::cl_RA> & , const FunctionJet<cln::cl_RA> & , int );
template cln::cl_N FaaDiBrunoFormula(const FunctionJet<cln::cl_N> & , const FunctionJet<cln::cl_N> & , int DerivativeOrder);
template cln::cl_N LeibnitzRule(const FunctionJet<cln::cl_RA> & f, const FunctionJet<cln::cl_N> & g, int DerivativeOrder);